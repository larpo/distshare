@DiscoveryCtrl = ($scope) ->
    $scope.servers = []
    $scope.selected = null

    sockId = null

    $scope.scan = ->
        $scope.servers.length = 0

        s2b = (str) ->
            buf = new ArrayBuffer(str.length)
            bufView = new Uint8Array(buf)

            for i in [0..str.length]
                bufView[i] = str.charCodeAt(i)

            return buf;

        b2s = (buf) ->
            String.fromCharCode.apply(null, new Uint8Array(buf))

        PORT = 1900
        ADDR = "239.255.255.250"
        MSG = s2b([
            "M-SEARCH * HTTP/1.1"
            "MAN:\"ssdp:discover\""
            "ST:urn:adhoc:service:PeerNode:1"
            "MX:1"
            ""
        ].join("\r\n"))

        timeout = null

        send = ->
            chrome.socket.sendTo sockId, MSG, ADDR, PORT, (sendStatus)->
                timeout = setTimeout send, 5000
                rcv()

        rcv = ->
            chrome.socket.recvFrom sockId, (data) ->
                clearTimeout timeout
                return if data.resultCode is -1
                rcv()

                $scope.$apply ->
                    addr = data.address
                    rows = b2s(data.data).split("\r\n")
                    for row in rows
                        r = row.split(":")
                        if r[0] is "ADHOC-PORT"
                            port = r[1]

                    addr = addr + ":" + port if port isnt 80

                    if $scope.servers.indexOf(addr) is -1
                        $scope.servers.push addr

        if not sockId
            chrome.socket.create 'udp', {}, (createInfo) ->
                sockId = createInfo.socketId

                chrome.socket.bind sockId, "0.0.0.0", 0, ->
                    send()

        else
            send()


    initBounds = null
    $scope.open = ->
        if not $scope.selected
            w = chrome.app.window.current()
            bounds = w.getBounds()
            if not initBounds
                initBounds = bounds

            WIDTH = 1280
            HEIGHT = 800
            w.setBounds
                width: WIDTH
                height: HEIGHT
                left: Math.round(bounds.left + bounds.width / 2 - WIDTH / 2)
                top: Math.round(bounds.top + bounds.height / 2 - HEIGHT / 2)

        $scope.selected = @server

        return false

    $scope.close = (e)->
        e.stopPropagation()

        if not $scope.selected
            chrome.app.window.current().close()
        else
            w = chrome.app.window.current()
            $scope.selected = null
            bounds = w.getBounds()
            w.setBounds
                width: initBounds.width
                height: initBounds.height
                top: Math.round(bounds.top + bounds.height / 2 - initBounds.height / 2)
                left: Math.round(bounds.left + bounds.width / 2 - initBounds.width / 2)
            $scope.scan()

    $scope.scan()


window.addEventListener 'DOMContentLoaded', ->
    el = document.querySelector '.grab'
    w = chrome.app.window.current()
    initialPos = null
    windowPos = null

    el.addEventListener 'mousedown', (e) ->
        windowPos = w.getBounds()
        initialPos =
            x: e.screenX
            y: e.screenY

    window.addEventListener 'mousemove', (e) ->
        return if not initialPos
        w.setBounds
            left: windowPos.left + (e.screenX - initialPos.x)
            top: windowPos.top + (e.screenY - initialPos.y)

    window.addEventListener 'mouseup', ->
        windowPos = null
        initialPos = null


"use strict"

module.exports = (MODULE_NAME, namespace="/") ->
    angular.module(MODULE_NAME + ".socket" , [])
        .factory 'socket', ($rootScope) ->
            socket = io.connect(namespace)

            _timeout = null
            _apply = ->
                $rootScope.$apply()
                _timeout = null
            apply = ->
                if not _timeout
                    _timeout = setTimeout _apply, 50
            obj =
                on: (event, fn) ->
                    socket.on event, (args...) ->
                        fn.apply(socket, [args...])
                        apply()

                emit: (event, data, fn) ->
                    socket.emit event, data, (args...)->
                        if fn
                            fn.apply(socket, [args...])
                            apply()

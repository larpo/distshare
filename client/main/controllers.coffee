"use strict"
ROOM = "/"
LIMIT = 50

module.exports = (MODULE_NAME) ->
    angular.module(MODULE_NAME + ".controllers", [])
        .controller MODULE_NAME, ['$scope', 'socket', '$location', ($scope, socket, $location) ->
            subscribed = $scope.subscribed = []
            services = []
            signsSeen = []
            $scope.data = {}

            markSubscriptions = () ->
                for s in services
                    s.subscribed = s.service in subscribed

            socket.on 'services', (data) ->
                services = data
                markSubscriptions()
                $scope.services = services

            socket.on 'connect', ->
                console.log "connected"
                socket.emit 'list'

                if window.location.pathname isnt "/"
                    subscribe window.location.pathname
                    $scope.standalone = true
                else
                    $scope.standalone = false
                    for path in $location.path().split(",")
                        if path and path isnt "/"
                            subscribe path
                        else
                            $scope.showServices = true

            socket.on 'data', (data) ->
                uri = data.service
                $scope.data[uri] ?= []
#                $scope.data[uri].unshift formatData(data.data, uri)
                #TODO: separate data from different hosts
                if data.sign not in signsSeen
                    signsSeen.push data.sign
                    time = data.data.datetime
                    last = $scope.data[uri][LIMIT]?.datetime
                    if last > time
                        return

                    i = 0
                    while $scope.data[uri][i]?.datetime > time
                        i++

                    $scope.data[uri].splice i, 0, data.data
                    $scope.data[uri] = $scope.data[uri][0..LIMIT]


            subscribe = (URI) ->
                console.log "subscribe to ", URI
                socket.emit "subscribe", URI
                subscribed.push URI

            unsubscribe = (URI) ->
                socket.emit "unsubscribe", URI
                i = subscribed.indexOf URI
                subscribed.splice i, 1
                delete $scope.data[URI]

            $scope.subscribe = (unsub) ->
                URI = @service.service
                if unsub then unsubscribe URI
                else subscribe URI

                $location.path subscribed.join(",")

                markSubscriptions()


        ]
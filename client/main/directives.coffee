"use strict"


module.exports = (MODULE_NAME) ->
    angular.module(MODULE_NAME + '.directives', [])
        .directive 'itemTable', ()->
            return {
            restrict: 'E'
            transclude: true
            scope:
                items: '='
                expand: '='
            templateUrl: '/partial/item-table'
            replace: true
            compile: (scope, element, attrs) ->
                formatTime = (time) ->
                    t = new Date(time) - new Date().getTimezoneOffset() * 60000
                    new Date(t).toISOString().replace("T", " ").split(".")[0]
                ($scope)->
                    $scope.isString = (val) ->
                        typeof(val) in ["string", "number"]

                    $scope.format = (val, key) ->
                        if typeof(val) is 'number'
                            if key is 'time'
                                return formatTime val
                            else
                                return Math.round(val * 100) / 100

                        if typeof(val) is 'string' and key is "datetime"
                            return formatTime val
                        return val
            }

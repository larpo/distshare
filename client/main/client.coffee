"use strict"

MODULE_NAME = "main"

require('../lib/socket')(MODULE_NAME)
require('./controllers')(MODULE_NAME)
require('./directives.coffee')(MODULE_NAME)

angular.module(MODULE_NAME, [MODULE_NAME+".socket", MODULE_NAME+".controllers", MODULE_NAME+".directives"])
    .config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
#        $locationProvider.html5Mode true
    ]
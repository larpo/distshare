#!/bin/bash
if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."
DEST=/var/www/adhoc/
mkdir -p ${DEST}

NODE_VERSION="v0.10.25"
if ! hash node 2>/dev/null || [ $(node -v) != ${NODE_VERSION} ]; then
    aptitude update && aptitude upgrade -y
    echo "updating nodejs"
    cd /tmp/
    curl http://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}.tar.gz | tar zx
    cd node-${NODE_VERSION}
    ./configure && make
    make install
    NEEDS_RESTART=1
fi


echo "Installing files from $DIR to $DEST"

cp -a ${DIR}/* ${DEST}
cd ${DEST}

#if [ ! -f "config.coffee" ]
#then
    cp config.coffee.sample config.coffee
#fi

#todo: update only if package version is different
npm update
npm install

if [ ! -f "/etc/init.d/adhoc" ]
then
    echo "setting up init script /etc/init.d/adhoc"
    cp ${DIR}/install/init_script /etc/init.d/adhoc
    chmod +x /etc/init.d/adhoc
    update-rc.d adhoc defaults
else
    cp ${DIR}/install/init_script /etc/init.d/adhoc
fi

cp ${DIR}/install/cron /etc/cron.d/adhoc

/etc/init.d/adhoc restart

if [ ${NEEDS_RESTART} ]
then
    reboot
fi
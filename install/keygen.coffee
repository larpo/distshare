fs = require('fs')
ursa = require('ursa')
location = __dirname + "/../keys/rsa.pem"

if not fs.existsSync location
    key = ursa.generatePrivateKey()
    fs.writeFileSync location, key.toPrivatePem()
    fs.writeFileSync location + ".pub", key.toPublicPem()
    console.log('Created keys in keys/rsa.pem');

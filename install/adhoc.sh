#!/bin/bash
#update script
DIR=/tmp/adhoc-update
rm -rf $DIR
mkdir -p $DIR
cd $DIR
curl https://larpo.kapsi.fi/adhoc.tar -o adhoc.tar
curl -s https://larpo.kapsi.fi/adhoc.sha1sum -o adhoc.sha1sum
echo -n "Checking shasum... "
shasum -s -c adhoc.sha1sum || { echo 'failure!' >&2; exit 1; }
echo "ok"
tar xf adhoc.tar
echo "Running installer"
cd adhoc/
./install/installer.sh

#!/usr/bin/env coffee
"use strict"
require 'coffee-script'
{UrlBot} = require __dirname + '/../lib/bot'
{dateString} = require __dirname + '/../lib/utils'

class SeismiOrgBot extends UrlBot
    SERVICE: "/earthquake/japan"
    INTERVAL: 1000*60*30
    KEYWORDS: ["earthquake", "seismic", "japan"]
    DESCRIPTION: "Seismic information of Japan from seismi.org"
    parse: JSON.parse
    URL: "http://www.seismi.org/api/eqs/?limit=200"
    format: (data) ->
        results = []
        for item in data.earthquakes
            if item.region?.match /japan/i
                item.datetime = @formatTime(item.timedate+"Z")
                item.source = "www.seismi.org"
                delete item.timedate

                results.push item

        return results

new SeismiOrgBot()

"""
sample data:
{
 "src": "us",
 "eqid": "b000i5wm",
 "timedate": "2013-07-03 15:06:11",
 "lat": "37.3199",
 "lon": "141.4182",
 "magnitude": "5.0",
 "depth": "16.20",
 "region": "near the east coast of Honshu, Japan"
}
"""
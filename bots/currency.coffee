#!/usr/bin/env coffee
"use strict"
require 'coffee-script'
{UrlBot} = require __dirname + '/../lib/bot'

lastRate = 0
sameCount = 0

class CurrencyBot extends UrlBot
    SERVICE: "/currency/eur/jpy"
    KEYWORDS: ["currency", "euro", "yen"]
    DESCRIPTION: "Latest euro to yen currency rates from openexchangerates.org"

    INTERVAL: 1000*60*60 #1h since data is updated at most hourly, and the api is limited to 1000 queries/month

    parse: JSON.parse
    URL: "http://openexchangerates.org/api/latest.json?app_id=a2a00588d150401ba30682fbea2e1d68"
    format: (data) ->
        result =
            datetime: @formatTime(data.timestamp)
            rate: Math.round(100*data.rates["JPY"] / data.rates["EUR"]) / 100
            source: "openexchangerates.org"


        #try to sync to when new data is available... increase interval when data haven't changed

        clearTimeout @_pollTimeout
        nextUpdate = @INTERVAL - (Date.now() - data.timestamp*1000) + 1000*60*5

        if lastRate is result.rate
            nextUpdate += @INTERVAL * Math.min(2*sameCount++, 8)
        else
            sameCount = 0

        @_pollTimeout = setTimeout @_poll, nextUpdate

        lastRate = result.rate

        return result

new CurrencyBot()
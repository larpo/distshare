#!/usr/bin/env coffee
"use strict"
require 'coffee-script'
{UrlBot} = require __dirname + '/../lib/bot'

lastData = null

class WeatherBot extends UrlBot
    SERVICE: "/weather/sendai"
    KEYWORDS: ["weather", "sendai"]
    DESCRIPTION: "Weather information for Sendai, provided by openweathermap.org"
    parse: JSON.parse
    URL: "http://openweathermap.org/data/2.1/weather/city/2111149?type=json&units=metric"
    format: (data) ->
        result =
            datetime: @formatTime(data.dt)
            location: "Sendai"
            currently:
                temperature: data.main.temp
                percipation: data.rain?["3h"]
                humidity: data.main.humidity / 100
                wind: data.main.wind?.speed
                description: data.weather?[0]?.description
            source: "openweathermap.org"

        return result

new WeatherBot()

###
    json target-format for weather data:

    {
        "time": 1365433200, // time received, unixtime
        "location": "Sendai",
        "currently": < weather object >,
        "hourly": [ < weather object1 >,  ... ], //hourly forecasts
        "daily": [ < weather object >, ... ], //daily forecasts
        "source": "openweathermap.org"
    }

    < weather object >

    {
        "time": 1365508800,
        "temperature": 12.3  // in C
        "percipation": 1.2 // mm
        "humidity": 0.4 // humidity [0,1]
        "wind": 5.1 // km/h
        "description": "Cloudy"
    }

###
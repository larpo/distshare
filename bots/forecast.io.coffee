#!/usr/bin/env coffee
"use strict"
require 'coffee-script'
{UrlBot} = require __dirname + '/../lib/bot'

class ForecastIoBot extends UrlBot
    SERVICE: "/weather/sendai"
    INTERVAL: 1000*60*30
    KEYWORDS: ["weather", "sendai"]
    DESCRIPTION: "Weather information for Sendai, provided by forecast.io"
    parse: JSON.parse
    URL: "https://api.forecast.io/forecast/e6ae8ab453e056726b72af32d871b91d/38.26889,140.871933?units=si"
    format: (data) ->
        result =
            location: "Sendai"
            datetime: @formatTime(data.currently.time)
            currently:
                temperature: data.currently.temperature
                percipation: data.currently.percipIntensity
                humidity: data.currently.humidity
                wind: data.currently.windSpeed
                description: data.currently.summary
            source: "forecast.io"
        return result

new ForecastIoBot()
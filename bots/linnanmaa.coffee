#!/usr/bin/env coffee
"use strict"
require 'coffee-script'
{UrlBot} = require __dirname + '/../lib/bot'
cheerio = require 'cheerio'

lastData = null

class WeatherBot extends UrlBot
    SERVICE: "/weather/oulu"
    KEYWORDS: ["weather", "oulu", "linnanmaa"]
    DESCRIPTION: "Weather information for Oulu, provided by http://weather.willab.fi/weather.html"
    URL: "http://weather.willab.fi/weather.html"
    parse: cheerio.load
    format: ($) ->
        data = {}

        data.temp = $('p.tempnow').text().split(" ")[0]

        ths = $('table tr th').map((i,el)->$(el).text())
        tds = $('table tr td').map((i,el)->$(el).text())

        for th,i in ths
            if th.match /humidity/i
                data.humidity = tds[i]?.split(" ")[0]
            else if th.match /wind speed/i
                data.wind = tds[i]?.split(" ")[0]

        result =
            datetime: @formatTime(Date.now())
            location: "Oulu"
            currently:
                temperature: data.temp
                humidity: data.humidity / 100
                wind: data.wind
            source: "http://weather.willab.fi/weather.html"

        return result

new WeatherBot()

#!/usr/bin/env coffee
"use strict"
require 'coffee-script'

forever = require 'forever'
path = require 'path'

N = 3
DONE = 0
env = process.env.npm_package_config_environment

file = './p2p-lite-cluster.coffee'
console.log "Starting #{N} x100 servers..."

for i in [1..N]
    opts =
        command: 'coffee'
        silent: true
        sourceDir: __dirname

    child = new (forever.Monitor)((if i is 1 then 'server.coffee' else file), opts)
        .on 'start', ->
            DONE++
            if DONE is N
                console.log "DONE"
        .on 'error', (err) ->
            console.log "error", file, err
        .on 'stderr', (err) ->
            console.log "stderr", file, err.toString()
        .on 'exit', ->
            console.log file, "exited after too many restarts"


#    forever.startServer child

    child.start()

exports.index = (req, res) ->
    res.render 'index'

exports.partial = (req, res) ->
    res.render 'partials/' + req.params.name
#!/usr/bin/env coffee
"use strict"
http = require 'http'
utils = require './lib/utils'

MAX=80
N=0

rm = (arr, value) ->
    i = arr.indexOf value
    while i != -1
        arr.splice i, value
        rm arr, value

start = () ->
    if N > MAX then return

    N++

    p2p = new (require './lib/p2p')()

    RTT = 20 + Math.random() * 50
    server = http.createServer utils.httpStub(p2p, RTT)

    port = 8090
    server.on 'listening', ->
        addr = utils.ipList().pop()
        id =  utils.hash JSON.stringify {port,addr}
        p2p.init {port, addr, id}

        console.log Date(Date.now()), ": (#{N}/#{MAX}) started on port #{port} ..."

        end = =>
            if Math.random() > 1 / (MAX*100)
                return

            clearInterval t
            server.close()
            p2p.close()
            N--

        t = setInterval end, 10*1000

    server.listen port

    server.on 'error', (e)->
        if e.code in ['EACCES', 'EADDRINUSE']
            port += Math.ceil Math.random() * Math.sqrt(port)
            server.listen port

    return p2p


_loop = ->
    if N < MAX then start()
    setTimeout _loop, (Math.log(N) / Math.LN2) * 1000

_loop()
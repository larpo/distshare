Storage = require '../lib/storage'
assert = require 'assert'
Promise = require 'bluebird'
describe "storage utility", ->
    st1 = new Storage()

    before (done) ->
        Promise.all([
            st1.add {sign: 123, time: Date.now(), service: '/foo/bar'}
            st1.add {sign: 321, time: Date.now(), service: '/foo/baz'}
        ]).then ->
            done()


    it "should save items", (done) ->
        st1.get()
        .then (results) ->
            assert.equal results.length, 2
            done()

    it "should get matching items correctly", (done) ->
        st1.get("/foo/bar")
            .then (results) ->
                assert.equal results.length, 1
                st1.get("/foo/*")
            .then (results) ->
                assert.equal results.length, 2
                st1.get("/foo/")
            .then (results) ->
                assert.equal results.length, 0
                done()

    it "should fire data event when saving", (done) ->
        st1.on 'data', ->
            done()

        st1.add {sign:444, time: Date.now(), service: '/bar/baz'}
P2P = require '../lib/p2p'
utils = require '../lib/utils'

describe "p2p tests", ->
    items = [
        {service: "/foo/bar"} #a82cce35fd860de6f63f97e6c482dc6a14d002e8
        {service: "/foo/baz"}
        {service: "/bar"}
        {service: "/bar/location"}
        {service: "/my/home"}
    ]

    peers = [
        {addr: "127.0.0.1", port: 9090, id: "b3669cc92b98ba5e24f3b52f7fe82bdac3bc2418"}
        {addr: "127.0.0.1", port: 9091, id: "d9b81d223403192c8f6a3ad0823c8c11e281a26a"}
        {addr: "127.0.0.1", port: 9092, id: "5d9281494cf48f78b494bd1f0470879b1a7c1b1a"}
        {addr: "127.0.0.1", port: 9093, id: "08b889744f110c3f91659fa406e6deec37d29e61"}
        {addr: "127.0.0.1", port: 9094, id: "f9b01f84d7f2dd4533c6882d5a24d06708cc3075"}
        {addr: "127.0.0.1", port: 9095, id: "5037c7894c7af75f245a1f0d4e513940bed701b2"}
        {addr: "127.0.0.1", port: 9096, id: "198e032f797565a36fa149ba4cf32534601e86cd"}
        {addr: "127.0.0.1", port: 9097, id: "dcb5ac7e1ff8a890ca41ae6fe1ce8a76b64b7f24"}
        {addr: "127.0.0.1", port: 9098, id: "1bc847e5455105c84e07a010d6b3691aab28280f"}
        {addr: "127.0.0.1", port: 9099, id: "353085058947550315b371cda73972a9e8bc768e"}
    ]

    p2pOpts = {test: true, nodiscovery: true}

    p2ps = []
    p2p = null

    before (done) ->
        ready = 0
        for peer,i in peers
            do (peer,i) ->
                _p2p = new P2P(null, p2pOpts)
                p2ps.push _p2p

                _p2p.me = peer

                http = require 'http'

                server = http.createServer utils.httpStub(_p2p)
                server.listen peer.port

                server.on 'listening', ->
                    #init peers with peer before and after it
                    l = peers.length
                    _p2p.addPeer peers[(i+1)%l]
                    _p2p.addPeer peers[(l+i-1)%l]

                    if ++ready is peers.length
                        p2p = p2ps[0]

                        p2p.register(items)
                        .then ->
                            done()


    describe "local api tests", ->

        it "should search properly", ->
            assert.equal p2p._searchLocal("*").length, items.length, "wildcard * should work"
            assert.equal p2p._searchLocal("/foo/bar").length, 1, "exact match should return 1 item"
            assert.equal p2p._searchLocal("/foo/*").length, 2, "/foo/* return 2 items"

        it "should match hashes for items", ->
            resItems = p2p._getServices "a82cce35fd860de6f63f97e6c482dc6a14d002e8"
            assert.equal resItems[0].service, "/foo/bar"

    describe "networked p2p tests", ->
        it "should find all peers", (done)->
            p7 = peers[7]
            p2p.getPeer(p7.id)
                .then ({peers}) ->
                    assert.equal p7.id, peers[0].id, "should find connected peer"
                    done()

        it "should be able to get services from other peers", (done) ->
            p2ps[4].getService(items[1].service)
                .then ({services}) ->
                    assert.equal services.length, 1, "should return only one result"
                    assert.equal services[0].peer.id, peers[0].id, "should have right peer"
                    done()

        it "should search", (done) ->
            p2ps[3].getPeer(peers[6].id)
                .then ->
                    p2ps[3].searchItem("/foo/*")
                .then ({services, items}) ->
                    assert.equal services.length, 2, "should find both /foo/bar and /foo/baz"
                    done()

        it "should subscribe and publish", (done) ->
            d =
                service: '/foo/bar'
                data:
                    datetime: '2013-07-13T16:01:09Z'
                    rate: 129.69
                    source: 'openexchangerates.org'
                sign: 'ac5feef7224528d0f4a961a02d5255418972321e'
                time: 1373734632734

            p2ps[4].subscribe("/foo/bar")
                .then ->
                    p2ps[0].publish d

            p2ps[4].cache.on 'data', (data)->
                assert.equal data.service, "/foo/bar"
                done()
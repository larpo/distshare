utils = require '../lib/utils'
assert = require 'assert'

describe "Utils test", ->
    describe "hash function", ->
        it "should hash strings", ->
            assert.equal utils.hash('test'), 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3'

    describe "matcher", ->
        match = utils.urlMatch
        it "should match exact strings", ->
            assert.equal match("/foo/bar", "/foo/bar"), true

        it "shouldn't match partially same strings", ->
            assert.equal match("/foo/bar", "/foo/"), false

        it "shouldn't match different strings", ->
            assert.equal match("/foo/bar", "/bar/foo"), false

        it "should match *", ->
            assert.equal match("/foo/bar", "*"), true

        it "should match * combination", ->
            assert.equal match("/foo/bar/baz", "/*/baz"), true


    describe "xor functions", ->
        {xor, bucketNo, sortByDistance} = utils

        it "should calculate correct xor of strings", ->
            h1 = ""
            h0 = ""
            h10 = ""
            for i in [1..40]
                h1 += "f"
                h0 += "0"
                h10 += "#{i%2}"

            #xor is reversible
            assert.equal xor(h0, h1), xor(h1, h0)

            #xor of same string  is 0
            assert.equal xor(h0, h0), h0
            assert.equal xor(h1, h1), h0
            assert.equal xor(h10, h10), h0

            assert.equal xor("ffff", "f000"), "0fff"
            assert.equal xor("8765", "efc0"), "68a5"
            assert.equal xor("1234", "abcd"), "b9f9"


        it "should calculate correct distance/bucket", ->
            assert.equal bucketNo("ffff", "7bbb"), 1
            assert.equal bucketNo("ffff", "aaaa"), 2
            assert.equal bucketNo("cccc", "ebbb"), 3
            assert.equal bucketNo("ffff", "ffcd"), 11
            assert.equal bucketNo("ffff", "ffff"), 16


        it "should sort peers based on xor distance", ->
            base = "ffff"
            peers = [
                {id: "ffff"}
                {id: "0000"}
                {id: "aaaa"}
                {id: "abcd"}
                {id: "1234"}
                {id: "ffcd"}
            ]

            correct = [
                {id: "ffff"}
                {id: "ffcd"}
                {id: "abcd"}
                {id: "aaaa"}
                {id: "1234"}
                {id: "0000"}
            ]

            sorted = sortByDistance base, peers
            for p,i in sorted
                assert.equal p.id, correct[i].id


    describe "cryptofunctions", ->
        it "should be able to verify correct signatre"
        it "should be able to verify incorrect signatre"
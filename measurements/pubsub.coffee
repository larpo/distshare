#!/usr/bin/env coffee
"use strict"
#require('nodetime').profile
#    accountKey: '9e5d2a32900e1cf44a75f7b1dd7859929d494d88'
#    appName: 'pubsub'

argv = require('yargs')
    .default('peers', 100).alias('n', 'peers')
    .default('churn', 2).alias('c', 'churn')
    .default('time', 1200).alias('t', 'time')
    .default('delay', 10).alias('d', 'delay')
    .default('bucket', 15).alias('b', 'bucket')
    .default('subsN', 3).alias('s', 'bucket')
    .default('log', false)
    .default('rerun', false)
    .default('nodelete', false)
    .boolean(['nodelete', 'log'])
    .argv


DELAY = argv.delay
N = argv.n
churn = argv.churn
#BUCKET_SIZE = Math.ceil(Math.log(N) / Math.LN2)
BUCKET_SIZE = argv.bucket


http = new (require '../lib/httpMock')({delay: DELAY})
#http = require 'http'
utils = require '../lib/utils'
Promise = require 'bluebird'
Promise.longStackTraces();

BOOTED = false
console.log "starting test with #{N} nodes with bucket size #{BUCKET_SIZE}"

START_PORT = 20050

global.NODES = NODES = []
DEADPEERS = []
#NODES = []
global.IDS = IDS = []

getDeadpeers = (node) ->
    dead = 0
    all = 0
    for n, buck of node.bucket
        all+= buck.length
        for p in buck when p.id in DEADPEERS
            dead++

    return {dead: dead, all: all, alive: (all - dead)}


SERVICES = []
getService = (i) ->
    return {service: "#{i}"}


pickPeer = ->
    alive = NODES.filter (n) -> n
    p = alive[(alive.length * Math.random()) | 0]
    if p.me.id in DEADPEERS
        p = pickPeer()

    return p

STATS =
    bootstrapTimes: []

avg = (arr,dec=2) ->
    sum = arr.reduce (item, left) ->
        item + left

    round sum / arr.length, dec

round = (num, dec=2) ->
    mul = Math.pow(10,dec)
    Math.round(num * mul) / mul

class MsgTracker
    msgs: null
    timeout: (5 + N / 10) * 1000
    subs: null
    closeResolvers: null

    sent: 0
    rcvd: 0
    time: null
    pubstat: null
    rtt: null

    constructor: ->
        @msgs = {}
        @subs = {}
        @closeResolvers = {}

        @reset()

    reset: ->
        @times = []
        @pubstat = []
        @avgtimes = []
        @rtt = []

        @sent = 0
        @rcvd = 0
        http.deletes = 0
        http.msgs = 0
        http.posts = 0
        http.heads = 0

    addMsg: (id) ->
        sub = id.slice 0, id.lastIndexOf('#')
        if (@subs[sub] ?= 0) < 1
            return

        t = setTimeout =>
            @_clearMsg(id,true)
        , @timeout

        @msgs[id] = {t, time: Date.now(), received:0}

    addSub: (id, onEnd) ->
        @subs[id] ?= 0
        @subs[id]++

        @closeResolvers[id] ?= []
        @closeResolvers[id].push onEnd


    rmSub: (id) ->
        if @subs[id]
            @subs[id]--

    closeSub: (id) ->
        for f in (@closeResolvers[id] || [])
            setTimeout f, 0

        delete @closeResolvers[id]

    _clearMsg: (id, timeout) ->
        sub = id.slice 0, id.lastIndexOf('#')

        if @subs[sub] > 0
            @sent++

        if timeout or @subs[sub] is @msgs[id].received
#            if timeout and @subs[sub]
#                console.log id, "timeouted subs:", @subs[sub]
#                _id = IDS[Number(sub)-1]
#                _sub = utils.hash sub
#                if not _id
#                    for id, i in IDS
#                        console.log "[#{i}]: #{id[0..8]}"
#
#                #publisher
#                if NODES[sub]
#                    NODES[sub].getPeer(_id)
#                        .then (results) ->
#                            console.log "#{IDS[sub][0..8]} (responsible) getPeer for #{_id[0..8]}:", results
#                            @getSubscribers _sub
#
#                        .then (results) ->
#
#                            console.log "#{IDS[sub][0..8]} (responsible) geSubscribers for #{_sub[0..8]}:", results
#
#                if NODES[Number(sub)-1]
#                    NODES[Number(sub)-1].getPeer(_sub)
#                        .then (results) ->
#                            console.log "#{_id[0..8]} had subscribed to:", @mySubscriptions
#                            console.log "#{_id[0..8]} getPeer for service #{_sub[0..8]}:", results

            if timeout and @subs[sub] isnt @msgs[id].received
                console.log "#{id} expected", @subs[sub], "but received", @msgs[id].received
                #publisher
#                _id = IDS[Number(sub)-1]
#                _sub = utils.hash sub
#
#                if NODES[sub]
#                    NODES[sub].getSubscribers(_sub)
#                        .then (results) ->
#                            console.log "#{IDS[sub][0..8]} (responsible) geSubscribers for #{sub} #{_sub[0..8]}:", results

            clearTimeout  @msgs[id].t
            delete @msgs[id]

    clearMsg: (id, time) ->
        @rtt.push time
        if not @msgs[id]
            console.log "received #{id} too late subs:", @subs[id.slice(0, id.lastIndexOf('#'))]
            return

        @msgs[id].received++
        @times.push (Date.now() - @msgs[id].time)
#        if (Date.now() - @msgs[id].time) > 5000
#            console.log "received #{id} after ", Math.round((Date.now() - @msgs[id].time) / 1000)

        @rcvd++
        @_clearMsg id

    addPubStat: (data) =>
        @pubstat.push data

    print: ->
        fail = @sent - @rcvd
        time = avg(@times, 0)
#        @avgtimes.push time
        @times.length = 0

        subs = Object.keys(this.subs).reduce((l, s) =>
          this.subs[s] + l
        , 0)

        console.log "avg time: #{time}ms, published #{@sent} of which failed #{fail} (#{round(fail*100/@sent)}%), subs #{subs}"
        console.log "msgs: #{http.msgs} of which deletes: #{http.deletes}, posts: #{http.posts}, pings: #{http.heads}"


        if @pubstat.length
            peerQueries = avg (p.peerQueries for p in @pubstat)
            queryTime = avg (p.queryTime for p in @pubstat)
            hopCount = avg (p.hopCount for p in @pubstat)
            maxQueryTime = Math.max.apply null, (p.queryTime for p in @pubstat)
            postTime = avg (p.postTime for p in @pubstat)
            maxPostTime = Math.max.apply null, (p.postTime for p in @pubstat)
            maxPeerQueries = Math.max.apply null, (p.peerQueries for p in @pubstat)
            msgs = avg (p.msgs for p in @pubstat)
            rtt = avg @rtt
            maxRtt = Math.max.apply null, @rtt
            alive = NODES.filter (n) -> n
            deadStats = alive.map getDeadpeers
            avgDead = avg (stat.dead for stat in deadStats)
            avgPeers = avg (stat.all for stat in deadStats)
            minPeers = Math.min.apply null, (stat.alive for stat in deadStats)

            console.log "#{peerQueries} msgs/publish (max: #{maxPeerQueries}), hops #{hopCount}"+
            "qtime: #{queryTime} / #{maxQueryTime} ptime: #{postTime} / #{maxPostTime}, rtt: #{rtt} / #{maxRtt}"
            console.log "avgPeers: #{avgPeers} (min #{minPeers}) avgDead: #{avgDead}"
            @pubstat.length = 0

        return {hopCount, fail, rtt, maxRtt, peerQueries, queryTime, maxQueryTime, postTime,
        maxPostTime, maxPeerQueries, msgs, sent:@sent, time,  avgDead, avgPeers,minPeers,
        httpMsg: http.msgs, httpDelete: http.deletes, httpHead: http.heads, httpPosts: http.posts}

tracker = new MsgTracker

start = ->
    port = START_PORT++
    opts =
        test: true
        nodiscovery: true
        mockHttp: http
        silent: true
        k: BUCKET_SIZE
        nodelete: argv.nodelete
        subscribeN: argv.subsN

    p2p = new (require '../lib/p2p')(null, opts)

    server = http.createServer utils.httpStub(p2p)

    server.once 'listening', ->
        addr = utils.ipList().pop()
#        id = utils.hash Date.now() + ":" + port
        id = utils.hash port
        p2p.init {port, addr, id}
        p2p.__server = server

        addPeer = ->
            alive = NODES.filter (n) -> n
            p = alive[(alive.length * Math.random()) | 0]?.me
            if p
                p2p.addPeer {addr: p.addr, port: p.port, id: p.id}

        addPeer()
        addPeer()

        NODES.push p2p
        IDS.push id

        boot = ->
            p2p.getPeer(id)
                .bind(p2p)
                .then ({peers}) ->
                    if @_getClosestPeers().length is 0
                        return Promise.reject {}

                    max = Math.max.apply(null, Object.keys(@bucket))
                    promises = []
                    for i in [1..Math.min(10,max)-1]
                        h = utils.hash ""+Math.random()
                        while i isnt utils.bucketNo(id, h)
                            h = utils.hash ""+Math.random()

                        promises.push @getPeer h


                    Promise.settle(promises)
                .error ->
                    addPeer()
                    addPeer()
                    setTimeout boot, 100
                .then ->
                    if @_getClosestPeers().length > 0
                        @emit 'bootstrapped'
                    else
                        console.log "WTF u y no peers?"




        boot()
        p2p.on 'close', ->
            if p2p.listeners('closed').length is 1
                console.log "WTF??"
        p2p.once 'bootstrapped', ->
            p2p.churn.length=0
            if BOOTED is false
                STATS.bootstrapTimes.push (Date.now() - @_initTime)
                if STATS.bootstrapTimes.length is N
                    BOOTED = true
                    for n in NODES
                        n.churn.length = 0
                    printBootStats()

            i = NODES.indexOf p2p
            service = getService(i)
            @register {service: service.service}

            closed = false
            service.seq = 0
            publish = =>
                if STATS.bootstrapTimes.length isnt N
                    setTimeout publish, 2000
                    return

                if closed
                    return

                tracker.addMsg service.service + "#" + service.seq

                @publish
                    service: service.service
                    time: Date.now()
                    seq: service.seq
                .then(tracker.addPubStat)

                service.seq++

                setTimeout publish, 2500 + Math.random() * 6000

            setTimeout publish, 8000

            _sub = getService(i+1).service

#            if BOOTED
#                console.log "#{@me.id[0..8]} subscribed for #{_sub} (#{utils.hash(_sub)[0..8]})" +\
#                "and started to serve #{service.service} (#{utils.hash(service.service)[0..8]})"

            _subscribe = (resub=false) =>
                if resub
                    _sub = getService(NODES.indexOf(pickPeer())).service

                    if _sub is service.service
                        _subscribe(true)
                        return

                    console.log "#{@me.id[0..8]} moving sub to new service! #{_sub}"


                p2p.subscribe(_sub)
                    .catch ->
                        console.log "failed peers detected while subscribing"
                    .then =>
                        if resub and false
                            p2p.getSubscribers(utils.hash _sub)
                                .then ({subscribers,peers}) =>
                                    _ids = subscribers.map (p) -> p.id
                                    if p2p.me.id not in _ids
                                        console.log "#{@me.id[0..8]} not in", _ids.map (id) -> id[0..8]
                                        console.log "responsible peers for #{_sub} #{utils.hash(_sub)[0..8]}", peers

                        tracker.addSub _sub, ->
                            if not closed
                                tracker.rmSub _sub
                                _subscribe(true)


            _subscribe()

            @on 'data', (data) ->
#                return true
                if data.service isnt _sub
                    console.log "#{@me.id[0..8]} received data from wrong service! #{data.service}, not #{_sub}"

                tracker.clearMsg data.service + "#" + data.seq, Date.now() - data.time

            rebootstraptimer = Date.now()
            @on 'reboot', =>
                if rebootstraptimer + 10000 > Date.now()
                    return

                console.log "rebooting"
                rebootstraptimer = Date.now()
                addPeer()
                addPeer()
                @getPeer @me.id
                @getPeer utils.hash(Math.random())

            @on 'close', =>
                if closed is true
                    console.log "WTF ALREADY CLOSED???"

                if NODES.indexOf(p2p) isnt i
                    console.log "wtf i is different????"
                closed = true
                tracker.rmSub _sub
                SERVICES[i] = null
                NODES[i] = null
                tracker.closeSub service.service

                server.close()

    server.listen port

    server.on 'error', (e)->
        if e.code in ['EACCES', 'EADDRINUSE']
            console.log "port in use"
            server.listen port++

    return p2p

for i in [1..N]
    start()

emulateChurn = ->
    if not BOOTED
        return

    if churn < 1 and churn < Math.random()
        return

    for i in [1..Math.ceil(churn)]
        peer = pickPeer()
        if peer.listeners('bootstrapped').length > 0
            console.log "nope, dont close that!"
            return
        console.log "failing peer #{peer.me.id[0..8]} (#{NODES.indexOf(peer)})"
        DEADPEERS.push peer.me.id
        peer.close()
        peer = start()
        console.log "starting new peer #{peer.me.id[0..8]} (#{NODES.indexOf(peer)})"

setInterval emulateChurn, 5*1000

#setTimeout ->
##    NODES[0].opts.verbose = true
#    console.log "closing peer #{NODES[10].me.id[0..8]}"
#    NODES[10].close()
#,10000

printBootStats = ->
    _avg = Math.round(avg(STATS.bootstrapTimes)) / 1000
    _max = Math.max.apply(null, STATS.bootstrapTimes) / 1000
    _min = Math.min.apply(null, STATS.bootstrapTimes) / 1000
    _estmSize = avg NODES.map((n)->n._estimateSize())
    _peers = avg NODES.map((n)->n._getClosestPeers().length)

    console.log "bootstrap done!"
    console.log "time: avg #{_avg}s min #{_min}s max #{_max}s, msgs: #{round(http.msgs / N, 1)} per peer"
    console.log "nodes estimated that the size of network is #{_estmSize} peers, avg #{_peers} peers in buckets"

hd = null
memwatch = require 'memwatch'

#memwatch.on 'leak', (info) ->
#    console.log 'leaking!', info
i = 0
while true
    del = ""
    if argv.nodelete
        del = "-nodelete"
    logfile = __dirname + "/../logs/pubsub-N#{N}-C#{churn}-B#{BUCKET_SIZE}-S#{argv.subsN}#{del}.#{i}.csv"
    i++
    exists = require('fs').existsSync logfile
    if exists and argv.log
        if not argv.rerun
            process.exit()
    else
        break

printStats = ->
    memwatch.gc()
    console.log new Array(45).join("-")

    mem = Math.round(process.memoryUsage().rss / Math.pow(10,6))
    console.log "runtime: #{utils.formatTime(process.uptime())}, memory usage: #{mem}M"

    if tracker.times.length > 0
        stats = tracker.print()
        delete stats.msgs
        tracker.reset()
        if argv.log
            fs = require 'fs'
            keys = Object.keys stats
            if not fs.existsSync logfile
                fs.appendFileSync logfile, "time\t"+ keys.join('\t') + "\r\n"

            row = keys.map((key) ->
                return stats[key]
            ).join('\t') + "\r\n"

            fs.appendFile logfile, process.uptime() + "\t" + row



setInterval printStats, 10*1000

setTimeout ->
    console.log "#########################\n"
    process.exit()
, argv.time * 1000

process.on 'SIGINT', ->
    printStats()
    process.exit()

process.on 'exit', ->


"""
tests:
 - set the initial nodes to register a service
   - subscribe to services from separate nodes
   - publish random data to services every N seconds
   - see if packets come through and what is the delay
   - record failure rate

 - http stub to record packet counts, msg type
 - p2p to record failure rates

"""
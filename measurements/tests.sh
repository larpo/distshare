#!/bin/sh
BASEDIR=$(dirname $0)

coffee $BASEDIR/pubsub.coffee -n 20 --bucket 20 --churn=0 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 1 --churn=0 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 2 --churn=0 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 3 --churn=0 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 5 --churn=0 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 10 --churn=0 --log

coffee $BASEDIR/pubsub.coffee -n 100 --bucket 20 --churn=0 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 20 --churn=0.3 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 20 --churn=2 --log
coffee $BASEDIR/pubsub.coffee -n 100 --bucket 20 --churn=2 --nodelete --log

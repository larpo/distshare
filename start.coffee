#!/usr/bin/env coffee
"use strict"
require 'coffee-script'

forever = require 'forever'
path = require 'path'

env = process.env.npm_package_config_environment
production = env is "production"
console.log "environment is", env

fs = require 'fs'
if fs.existsSync './config.coffee'
    {files} = require './config.coffee'
else
    files = [
        './server.coffee'
    ]

for file in files
    do (file) ->
        bname = path.basename file
        opts =
            max: 0
            command: 'coffee'
            silent: production
            sourceDir: __dirname
            watch: false
            watchIgnoreDotFiles: true
            watchIgnorePatterns:    ['!/**/*.coffee', 'client/**']
            logFile: __dirname + '/logs/' + bname + '.log'
            outFile: __dirname + '/logs/' + bname + '.stdout.log'
            errFile: __dirname + '/logs/' + bname + '.stderr.log'

        child = new (forever.Monitor)(file, opts)
            .on 'start', ->
                console.log file, "started"
            .on 'restart', ->
                console.log file, "restarted"
            .on 'error', (err) ->
                console.log "error", file, err
            .on 'stderr', (err) ->
                console.log "stderr", file, err.toString()
            .on 'exit', ->
                console.log file, "exited after too many restarts"


        forever.startServer child

        child.start()

#!/usr/bin/env coffee
"use strict"
require 'coffee-script'
io = require 'socket.io'
express = require 'express'
http = require 'http'
browserify = require 'browserify'
#deopt = require 'browserify-deoptimizer'

discover = require './lib/discover'
storage = new (require './lib/storage')({persistent: true})

p2p = new (require './lib/p2p')(storage)
router = new (require './lib/router')(storage)

router.on 'search', (string) ->
#    p2p.searchItem(string)

router.on 'register', (service) ->
    p2p.register service

router.on 'subscribe', (string) ->
    p2p.subscribe string

router.on 'data', (data) ->
    p2p.publish data

routes = require './routes'
#process.env.NODE_ENV ?= "production"

client = browserify(
    mount: "/client.js"
    watch: true
    debug: true
).addEntry "client/main/client.coffee"

app = express()
if process.env.npm_package_config_environment is "production"
    app.set 'env', 'production'

server = http.createServer app
io = io.listen server
router.listen io

io.configure ->
    io.set 'log level', 2
    io.enable 'browser client minification'
    io.enable 'browser client gzip'

io.configure 'production', ->
    io.set 'log level', 1

app.configure ->
    app.use express.compress()
    app.use express.favicon()
    app.set 'views', __dirname + '/views'
    app.set 'view engine', 'jade'

    app.use client
    app.use '/p2p', p2p.serve
    app.use '/static', express['static'] __dirname + '/static'
    app.use app.router

#app.get '/', routes.index
app.get '/partial/:name', routes.partial
app.get '*', routes.index


#port = process.env.PORT or if app.get('env') is "production" then 80 else  8080
port = 80
server.on 'listening', ->
    addr = require('./lib/utils').ipList().pop()
    id =  require('./lib/utils').getId()
    version = process.env.npm_package_version

    discover.server {port, addr, id, version}
    p2p.init {port, addr, id}

    console.log Date(Date.now()), ": Node server #{process.env.npm_package_name} #{version} started on port #{port} ..."

server.listen port

server.on 'error', (e)->
    if e.code in ['EACCES', 'EADDRINUSE']
        if port is 80
            port = 8080
        else
            port += Math.ceil Math.random() * Math.sqrt(port)

        server.listen port
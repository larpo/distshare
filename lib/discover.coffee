"use strict"
udp = require 'dgram'
SSDP = require './ssdp'

SERVICE = "urn:adhoc:service:PeerNode:1"
PREFIX = "ADHOC-"
#listens to UPnP port 1900 on multicast address 239.255.255.250
TTL = 2

discover = (debug=false) ->
    defer = require('bluebird').defer()

    hosts = []
    clean = null
    timeout = null

    localIps = require('./utils').ipList()

    hello = new Buffer SSDP.search SERVICE

    if debug then console.log "sending\n", hello

    client = udp.createSocket("udp4")
        .on "message", (msg, rinfo) ->
            msg = SSDP.parse msg.toString()

            if debug then console.log "received \n", msg

            peer = {}
            for key, val of msg
                if key.match(PREFIX)
                    peer[key.replace(PREFIX, "").toLowerCase()] = val

            peer.addr = rinfo.address
            peer.local = rinfo.address in localIps

            for host in hosts
                return if host.addr is peer.addr and host.port is peer.port

            hosts.push peer
            defer.progress peer

            if clean is null
                clean = setTimeout =>
                    clearTimeout timeout
                    @close()
                    defer.resolve hosts
                , 20000

    send = ->
        client.send hello, 0, hello.length, SSDP.PORT, SSDP.ADDR
        timeout = setTimeout send, 8000

    send()

    return defer.promise


genResponse = (opts) ->
    params = {}
    for key, val of opts
        params[PREFIX + key] = val

    new Buffer SSDP.advertise("uuid",SERVICE, params)

server = (opts) ->
#    response = new Buffer JSON.stringify(opts)
    defip = opts.addr
    sock = udp.createSocket("udp4")
    sock.on "listening", ->
            addr = @address()
#            console.log "starting udp discovery server at " + addr.address + ":" + addr.port
            @addMembership SSDP.ADDR
            @setMulticastTTL TTL
        .on "message", (msg, rinfo) ->
            msg = msg.toString()
            parsed = SSDP.parse msg
            if parsed.ST in [SERVICE, "ssdp:all"]
#                console.log new Date(), "received", msg, "from host #{rinfo.address}:#{rinfo.port}"
                addr = defip
                for ip in require('./utils').ipList()
                    if ip[0..6] is rinfo.address[0..6]
                        addr = ip

                opts.addr = addr
                response = genResponse(opts)
                sock.send response, 0, response.length, rinfo.port, rinfo.address
        .bind SSDP.PORT

module.exports = {discover, server}

if require.main is module
    console.log "Probing for servers to connect"
    discover(true)
        .progressed (msg) ->
            console.log "discovered host #{msg.addr}:#{msg.port} local:#{msg.local}"

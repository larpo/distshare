"use strict"

Promise = require 'bluebird'

onError = (err) ->
    #    throw err
    console.log "fetchUrl failed:", err

fetchUrl = (opts, callback) ->
    defer = Promise.defer()

    if typeof opts is typeof {}
        {url, lastTime} = opts
    else
        url = opts


    url = require('url').parse(url)
    proto = url.protocol.replace(":", "")
    if proto is 'http'
        client = require 'http'
    else if proto is 'https'
        client = require 'https'
    else
        err = new Error "Protocol not supported " + proto
        defer.reject err

    if lastTime
        url.headers =
            "if-modified-since": lastTime

    client.get url, (res) ->
        if res.statusCode isnt 200
            console.log "responded with header", res.statusCode
            return

        data = ""
        res.on 'data', (chunk) ->
            data += chunk

        res.on 'end', ->
            defer.resolve {data, time: res.headers.Date}

        .on "error", onError

    return defer.promise


hash = (data) ->
    h = require('crypto').createHash 'sha1'
    if typeof(data) isnt "string"
        data = JSON.stringify data

    h.update data
    return h.digest 'hex'


keys = null

getKeys = ->
    if not keys
        ursa = require('ursa')
        filename = __dirname + '/../keys/rsa.pem'
        file = require('fs').readFileSync filename
        priv = ursa.createPrivateKey file
        keys = {priv, pub: ursa.coercePrivateKey(priv)}

    return keys

sign = (msg) ->
    key = getKeys().priv
    return key.sign('sha1', hash msg).toString('base64')

verify = (msg, sign) ->
    key = getKeys().pub
    return key.verify('sha1', hash msg, new Buffer(sign, 'base64'))

getId = ->
    hash getKeys().priv.toPublicPem().toString()

pat2regx = (pattern) ->
    s = pattern.replace /\*/g, ".*"
    return new RegExp '^' + s + '$'

urlMatch = (URI, pattern) ->
    return !!URI.match pat2regx(pattern)

ipList = () ->
    ips = []
    for name,iface of require('os').networkInterfaces()
        for addr in iface
            ips.push addr.address

    return ips

UUID = (data) ->
    h = hash(data)
    return "#{h[0..7]}-#{h[8..11]}-#{h[12..15]}-#{h[16..31]}"

dateTime = (time) ->
    if typeof time is "number" and String(time).length is 10
        time *= 1000
    new Date(time).toISOString().split(".")[0] + "Z"

formatTime = (s) ->
    m = Math.floor(s / 60)
    h = Math.floor(m / 60)
    s = s % 60
    res = "#{s}s"
    if m
        res = "#{m}min #{res}"
    if h
        res = "#{h}h #{res}"

    return res

httpStub = (p2p, delay = 0) ->
    (req, res) ->
        req.url = req.url.match(/^\/p2p(.*)/)?[1]
        if req.url
            req.ip = req.socket?.remoteAddress

            #add random delay
            process.nextTick ->
                p2p.serve(req, res)
        else
            res.end()


isHash = (h) -> h.search(/[0-9a-f]{40}/) is 0

xor = (id1, id2) ->
    d = ""
    for i in [0..id1.length-1]
        d+= (parseInt(id1[i], 16) ^ parseInt(id2[i], 16)).toString 16

    return d

#returns the position of first differing bit [1..160]
bucketNo = (id1, id2) ->
    bit = 0
    for char, i in id1
        if char is id2[i]
            bit += 4
            continue

        d = parseInt(char, 16) ^ parseInt(id2[i], 16)
        for i in [0..3]
            bit++
            if parseInt(d, 16) & (0x8 >> i)
                return bit

    return bit


sortByDistance = (base, peers=[], ignored=[]) ->
    sorted = []
    for peer in peers when peer.id not in ignored
        sorted.push {xor: xor(base[0..3], peer.id[0..3]), peer}

    sorted.sort (a,b) ->
        if a.xor > b.xor
            return 1
        else
            return -1

    return (p.peer for p in sorted)

module.exports = {hash, fetchUrl, ipList, urlMatch, dateTime, pat2regx,
    httpStub, UUID, formatTime, sign, verify, getId, isHash, xor, bucketNo, sortByDistance}
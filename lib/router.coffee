{urlMatch, hash} = require './utils'
EventEmitter = require('events').EventEmitter


#TODO: service objects as JSON-LD?
"""
Examples for service/subscription object
service:
{
    service: "/weather/sendai",
    id: "hash(service)",
    source: "http://forecast.io",
    description: "Weather in Sendai.",
    keywords: ["weather", "sendai"],
    provider: socket.id
}

subscription:
{
    service: "/weather/sendai"
    socket: socket
}

Router events:
    * register (service)
        emitted when peer registers new service

    * subscribe (service)
        emitted when peer subscribes to service

    * search (keyword, socket.id)
        emitted when peer searches something

    * data (data)
        emitted when local data is stored to cache/storage
  """

#todo: add sequence numbers to services so that it is possible to deduce missing packets
class Router extends EventEmitter
    services: null
    subscriptions: null
    cache: null
    socket: null

    constructor: (storage) ->
        @services = []
        @subscriptions = []
        @cache = storage || new (require './storage')()

        @cache.on 'data', @onData

        @cache.get()
            .then (items) =>
                services = items.reduce((arr, item)->
                    if arr.indexOf(item.service) == -1
                        arr.push item.service
                    return arr
                , [])
                for service in services
                    @addService(service)

    listen: (io) ->
        @socket = io.sockets

        self = @
        services = @services
        cache = @cache
        subscriptions = @subscriptions

        @socket.on 'connection', (socket) ->
            socket.subscribes = []
            socket.services = []

            socket.on 'data', (data) ->
                if data.to
                    @socket.sockets[data.to].emit "data", data
                else
                    cache.add data, true

            socket.on 'register', (service) ->
                self.emit 'register', service

                service.provider = socket.id if not service.provider
                service.id = hash service.service
                services.push service
                socket.services.push service

            socket.on 'subscribe', (service) ->
                if socket.subscribes.indexOf(service) is -1
                    console.log "subscribe for", service
                    self.emit 'subscribe', service

                    # bootstrap subscription by searching for matching past events
                    if subscriptions.every((s) -> return s.service isnt service)
                        self.emit 'search', service

                    subscriptions.push {service, socket}
                    socket.subscribes.push service

                    cache.get(service)
                    .then (results) ->
                        for data in results
                            socket.emit "data", data

                else
                    console.log "already on", service

            socket.on 'list', ->
                socket.emit "services", services


            socket.on 'unsubscribe', (service) ->
                subscriptions = subscriptions.filter (obj) ->
                    return (obj.socket isnt socket) or (obj.service isnt service)

                i = socket.subscribes.indexOf(service)
                if i > -1
                    socket.subscribes.splice i, 1

            socket.on 'disconnect', ->
                console.log "clearing subs/services for", socket.id
                subscriptions = subscriptions.filter (obj) ->
                    return obj.socket isnt socket

                if socket.services.length > 0
                    services = services.filter (obj) ->
                        return obj.provider isnt socket.id

                io.sockets.emit "services", services

    addService: (service) =>
        list = @services.map (service) -> service.service
        if service not in list
            @services.push {service}

    onData: (data, local) =>
        if not local
            @addService(data.service)
            console.log "received data", data

        for sub in @subscriptions
            if urlMatch(data.service,  sub.service)
                sub.socket.emit "data", data
            else
                console.log "not emiting because", data.service, "doesnt match", sub.service

        if local
            @emit 'data', data

module.exports = Router

http = require 'http'
sty = require 'sty'
{EventEmitter} = require 'events'
Promise = require 'bluebird'
Promise.longStackTraces();


{hash, isHash, bucketNo, sortByDistance} = require './utils'

#sha1 length in bits
IDLEN = 160
SUBSCRIBE_N = 2 #how many peers to save info to (actually +1, meaning 3)

P2P_NAMESPACE = '/p2p'

merge = (arr1, arr2) ->
    ids1 = arr1.map (p) -> p.id
    filt = (p for p in arr2 when p.id not in ids1)
    return arr1.concat filt

class p2p extends EventEmitter
    """
    Kademlia based DHT implementation that uses HTTP requests as transport protocol
    Check README for detailed description
        """

    services: null
    ownServices: null
    cache: null
    churn: null

    mySubscriptions: null
    subscribers: null
    closeIDCache: null

    bucket: null
    me: null
    up: null
    T: 30 * 1000
    k: 5

    constructor: (storage, @opts={}) ->
        @up = Date.now()

        @_clean()
        @cache = storage || new (require './storage')()

        if @opts.mockHttp
            http = @opts.mockHttp

        if @opts.k
            @k = @opts.k

        if @opts.subscribe_n
            SUBSCRIBE_N = @opts.subsribe_n - 1

    init: (me) ->
        if @me?
            @_clean()

        @me = me
        @_initTime = Date.now()

        if not @opts.silent
            console.log sty.green "starting p2p with id #{me.id}"

        if not @opts?.nodiscovery
            @_bootstrap()

        if @opts.verbose
            console.log "#{@me.id[0..8]}starting with verbose on!"

        if @opts.m
            @T = @opts.m * 1000

        @_maintananceInterval = setInterval @_maintanance, @T + Math.round(Math.random()*2000 -1000)

    _clean: ->
        clearInterval @_maintananceInterval
        @services = {}
        @ownServices = {}
        @bucket = {}
        @_searchItemIDs = []
        @churn = []
        @subscribers = {}
        @mySubscriptions = {}
        @closeIDCache = {}

    _estimateSize: ->
        closest = @_getClosestPeers()[0..@k]
        if closest.length > 0
            avg = closest.map((p)-> p.bucket).reduce((a, b) -> a + b) / closest.length
            return  Math.pow(2, avg) * (closest.length+1)/4
        else
            return 2


    _maintanance: =>
        M = rmInt = rmInt60 = joinInt = joinInt60 = 60 * 1000
        freq = (time, n) ->
            time / n

        removed = (item for item in @churn when item.removed)
        if removed.length > 0
            rmInt = freq Math.max(M, Date.now() - @up), removed.length
            rmInt60 = freq M, (item for item in removed when item.time + M > Date.now()).length

#        joined = (item for item in @churn when item.added)
#        if joined.length > 0
#            joinInt = freq Math.max(M, Date.now() - @up), joined.length
#            joinInt60 = freq M, (item for item in joined when item.time + M > Date.now()).length


        estmN = @_estimateSize()

#        int = Math.min(rmInt + rmInt60, joinInt + joinInt60) / 2
        int = (rmInt + rmInt60) / 2
#        console.log "rmInt #{rmInt|0}, #{rmInt60|0} join #{joinInt|0}, #{joinInt60|0} -> #{int|0}"
        interval = int * Math.pow((Math.log(estmN)/Math.LN2),2) / estmN

        pingPeers = =>
            if @opts.verbose
                console.log "ping interval: ", Math.round(interval / 1000), "int:", int

#            interval = Math.min(@T*10, Math.max(@T / 4, interval))
            interval = @T

            for n,b of @bucket
                if b[0] and (b[0].lastSeen + interval) < Date.now()
                    if @opts.verbose
                        console.log "#{@me.id[0..8]} pinging peer #{b[0].id[0..8]} in maintenance"
                    #@ping b[0]
                    @getPeer b[0].id, 0

#            @getPeer(hash(Math.random()))
#                .then =>
#                    @getPeer @me.id
#                .then =>
#

#        if Math.random < 0.1
#            @getPeer @me.id
#        else

        pingPeers()

        #check subscribe status, update my subscriptions to new peers if need be
        for id, subs of @mySubscriptions
            closest = @_getClosestPeers(id)[0..SUBSCRIBE_N]

    _bootstrap: =>
        if @opts?.nodiscovery
            @emit 'reboot'
            return

        first = true

        require('./discover').discover()
            .progressed (server) =>
                if server.id is @me.id then return
                if first
                    first = false
                    @addPeer server
                    @getPeer(@me.id)
                        .then =>
                            max = Math.max.apply(null, Object.keys(@bucket))
                            promises = []
                            for i in [1..max-1]
                                h = hash Math.random()
                                while i isnt bucketNo @me.id, h
                                    h = hash Math.random()

                                promises.push @getPeer h

                            Promise.settle promises
                        .then =>
                            #todo: search random ID from each of the buckets
                            #@bucket
                            @emit 'bootstrapped'
                else
                    @addPeer server


    close: ->
        if not @opts.silent
            console.log sty.red "closing server #{@me.id}"

        @closed = true
        @emit 'close'
        @removeAllListeners 'data'
        @_clean()

    _getServices: (key) ->
        if key
            services = @services[key] || []
        else
            services = (service for id, service of @services)
        return services

    _getClosestPeers: (id=@me.id, include=null) =>
        peers = []
        _bucket = bucketNo id, @me.id
        if @bucket[_bucket] and @bucket[_bucket].length > Math.min(@k, 10)
            peers = @bucket[_bucket]
        else
            for n, buck of @bucket
                peers = peers.concat buck

        if include
            peers = peers.concat [include]

        peers = peer for peer in peers when peer and peer.failed is null
        return sortByDistance id, peers

    _addServices: (services, from=@me) ->
        for service in services
            service.peer ?= from
            row = @services[hash service.service] ?= []

            if row.every((itm) -> service.peer.id isnt itm.peer.id)
                row.push service

    _searchLocal: (search) ->
        match = require('./utils').urlMatch
        services = [].concat (service for key,service of @services)...
        services = services.filter (service) ->
            return match service.service, search

        return services

    removePeer: (peer, purge=true) ->
        if @closed
            return

        if @opts.verbose
            console.log "#{@me.id[0..8]} remove peer #{peer.id[0..8]} called!"

        bucket = @bucket[peer.bucket]
        if not bucket
            console.error @me.id,"bucket N was", peer.bucket, "and peer", peer, "and bucket", @bucket
            return

        for _hash, subs of @subscribers
            #mark subscriber dead
            for s in subs
                if s.id is peer.id
                    s.failed = Date.now()

            #migrate subscribed peers to next neighbor if one is available
            if not @closeIDCache[_hash]
                @closeIDCache[_hash] = @_getClosestPeers(_hash, @me)[0..SUBSCRIBE_N].map((p)->p.id)

            _closeids = @closeIDCache[_hash]
            if @me.id in _closeids and peer.id in _closeids and peer.id in bucket.map((p)->p.id)
                delete @closeIDCache[_hash]
                nextpeer = @_getClosestPeers(_hash, @me)[SUBSCRIBE_N + 1]
                if nextpeer
                    if @opts.verbose
                        console.log sty.red "#{@me.id[0..8]} copying subscribed (#{_hash[0..8]}) peers to next peer", nextpeer.id[0..8]
                    @_subscribe nextpeer, _hash, subs


        peer.failed = Date.now()
        if purge
            bucket.splice bucket.indexOf(peer), 1

        @churn.unshift {id: peer.id, removed: true, time: Date.now()}

        if peer.lastSeen
            if not @opts.silent
                console.log sty.red "removed #{peer.id} (#{peer.addr}:#{peer.port})"

            #start discovery if no non-local peers are alive
            peers = []
            for n, buck of @bucket
                peers = peers.concat buck

            if peers.every((peer) -> return peer.local) or peers.length is 0
                @_bootstrap()


    # Returns false if peer is known to be dead
    addPeer: (peer, confirmed=true) =>
        if @closed or not peer.id
            return false

        if peer.id is @me.id
            return true

        delete peer.local

        n = bucketNo @me.id, peer.id
        peer.bucket = n

        buck = @bucket[n] ?= []

        #do not add peer to bucket if it is there already
        #instead update lastseen and move to bottom of the bucket
        for p,i in buck
            if p.id is peer.id
                #use old "good" ip if one exists, peers some times
                #have ips that are for unreachable interfaces etc
                if peer.addr isnt p.addr and p.lastSeen and peer.addr
                    peer.addr = p.addr

                p.lastSeen = Date.now()
                buck.push buck.splice(i,1)[0]
                return true

        for p in @churn
            if Date.now() - p.time > 5*60*1000
                break

            if p.id is peer.id and p.removed
                return false

        if confirmed
            peer.lastSeen = Date.now()
            delete peer.failed
        else
            peer.lastSeen = 0

        add = =>
            if peer.id in buck.map((p)->p.id)
                return

            if confirmed and not @opts.test
                console.log sty.green "added #{peer.id} (#{peer.addr}:#{peer.port})"

            buck.push peer
            #todo: mark the real join time for churn calculations?
            @churn.unshift {id: peer.id, added: true, time: Date.now()}


        #check if there is space in bucket, and try to make space if its full
        if buck.length < @k
            add()
#        else if buck[0].lastSeen + @T*2 < Date.now()
#            @ping buck[0], add

        #migrate subscribed peers to new closer neighbor if one appears
        for _hash, peers of @subscribers
            if peers.length is 0
                return

            if not @closeIDCache[_hash]
               @closeIDCache[_hash] = @_getClosestPeers(_hash, @me)[0..SUBSCRIBE_N].map((p)->p.id)

            _peers = [peer].concat @closeIDCache[_hash].map((id)->{id})
            closest = sortByDistance(_hash, _peers)[0..SUBSCRIBE_N].map((p)->p.id)

            if peer.id in closest and @me.id in closest
                delete @closeIDCache[_hash]
                if @opts.verbose
                    console.log sty.green "#{@me.id[0..8]} migrate sub #{_hash[0..8]}to new peer #{peer.id[0..8]}"
                add()
                @_subscribe peer, _hash, peers


        return true

    serve: (req, res) =>
        if @closed
            return
        {headers, method, ip} = req

        peer =
            id: headers["p2p-peer-id"]
            port: headers["p2p-peer-port"]
            addr: ip

        res.setHeader "p2p-peer-id", @me.id
        res.setHeader "p2p-peer-port", @me.port

        if peer.id then @addPeer peer

        if method is 'HEAD'
            res.end()
            return

        URL = require('url').parse req.url, true

        split = URL.pathname.toLowerCase().substring(1).split('/')
        action = split.shift()

        data = ""
        req.on 'data', (d)->
            data += d

        onData = (fn) =>
            req.on 'end', ->
                try
                    data = JSON.parse data
                    fn data
                catch e
                    console.log "FAILED TO HANDLE DATA", data, e
                res.end()

        switch action
            when 'peers'
                ID = split[0]
                if ID
                    peers = @_getClosestPeers(ID)[0]
                    if ID is 'self'
                        peers = @me
                    else if not peers or peers.id isnt ID
                        res.statusCode = 404
                        res.end()
                        return

                    if method is "DELETE"
                        if peers.lastSeen < (Date.now() - 5000)
                            if @opts.verbose
                                console.log "#{@me.id[0..8]} was told to remove peer with DELETE"

                            @ping peers

                        res.end()
                        return
                else
                    if URL.query.sort
                        peers = {peers: @_getClosestPeers(URL.query.sort)[0..@k]}
                    else
                        peers = {peers: @_getClosestPeers(@me.id)}

                res.end JSON.stringify(peers, null, 2)

            when 'services'
                ID = split[0]
                ID2 = split[1]
                if not ID2
                    if method is "GET"
                        #TODO /services/ID/PEER_ID
                        result =
                            peers: @_getClosestPeers(ID)[0..@k]
                            services: @_getServices ID

                        if result.services.length is 0
                            res.statusCode = 404

                        res.end JSON.stringify(result, null, 2)
                    else if method is "POST"
                        onData (services) =>
                            @_addServices services, peer
                else if ID2 is 'subscribers'
                    subs = @subscribers[ID] || []
                    if method is 'GET'
                        subs = (p for p in subs when not p.failed)
                        if @mySubscriptions[ID]
                            subs = merge subs, [@me]

                        result =
                            subscribers: subs,
                            peers: @_getClosestPeers(ID)[0..@k]

                        if  @_getServices(ID).length is 0
                            res.statusCode = 404

                        res.end JSON.stringify(result, null, 2)
                    else if method is 'POST'
                        onData (peers) =>
                            if not Array.isArray(peers)
                                peers = [peers]

                            for peer in peers
                                @addPeer peer, false

                            @subscribers[ID] = merge peers, @subscribers[ID] || []


            when 'search'
                bcastId = headers["p2p-bcast-id"]
                bcastBucket  = headers["p2p-bcast-bucket"]

                reqURI = split.join('/')
                #TODO: add visited peer information to results
#                console.log "received search for", reqURI
                @searchItem(reqURI, bcastBucket, bcastId)
                    .then (results) ->
                        res.end JSON.stringify(results, null, 2)

            when 'data'
                onData (data) =>
                    @cache.add data, false
                    @emit 'data', data

            when ''
                res.end JSON.stringify {
                    '@context': 'Main.jsonld'
                    'peers': '/peers'
                    'services': '/services'
                    'me': '/peers/me'
                }
            else
                msg = "404! #{URL.pathname} not found"
                console.log
                res.statusCode = 404
                res.end(msg)


    # msg (peer, path, params)
    #   optional parameters:}
    #   method: request method, defaults to GET
    #   msg: body msg, defaults to none
    #   headers: dict of header lines to put into headers
    msg: (peer, path, params={}) ->
        if @closed
            Promise.reject {}

        method = params.method || 'GET'

        @addPeer peer, false

        opts =
            method: method
            hostname: peer.addr
            port: peer.port
            path: P2P_NAMESPACE + path
            headers:
                "p2p-peer-id": @me.id
                "p2p-peer-port": @me.port

        for key,val of (params.headers || {})
            opts.headers[key] = val

        if params.msg
            opts.headers['Content-Type'] = 'application/json'
            opts.headers['Content-Length'] = params.msg.length

        new Promise (resolve, reject) =>
    #        console.log "#{@me.id} send msg to #{peer.id} ", path
            #TODO: message throttling / concurrence limit
            req = http.request opts, (res) =>
                data = ""
                res.on 'data', (chunk) ->
                    data += chunk

                res.on 'end', =>
                    if data
                        try data = JSON.parse data

                    resId = res.headers["p2p-peer-id"]
                    if resId and peer.id and resId isnt peer.id
                        req.emit 'error', {}
                    else
                        if not peer.id and resId
                            peer.id = resId
                            @addPeer peer, true

                        peer.lastSeen = Date.now()

                        resolve data


            req.on 'error', (e) =>
                #remove failed peer
                @removePeer peer
                emsg =
                    peer: peer
                    code: e.code
                    errno: e.errno

                reject emsg

            if params.msg
                req.write params.msg

            req.end()

    _get: (id, URI, concurrency=2) =>
        defer = Promise.defer()

        oldPeers = []
        peers = @_getClosestPeers(id)[0..@k]
        failedPeers = []
        queriesSent = 0

        fromWho = {}

        services = []
        subscribers = []
        hops = 0

        query = =>
            cnt = 0
            closest = sortByDistance(id, peers, failedPeers)[0..concurrency]
            if closest.length is 0 and not @closed
                @_bootstrap()

            for p in closest
                if not p
                    console.log "p not defined in #{@me.id[0..8]}"

            filtered = (p for p in closest when p.id not in oldPeers)

            if filtered.length
                hops++

            for peer in filtered
                oldPeers.push peer.id
                do (peer) =>
                    @msg(peer, URI)
                        .then (data) =>
                            if data.peers
                                _old = oldPeers.concat peers.map((p)->p.id)
                                for p in data.peers
                                    fromWho[p.id] ?= []
                                    fromWho[p.id].push peer

                                    if p.id not in _old
                                        if @addPeer(p, false)
                                            peers.push p
                                        else
                                            failedPeers.push p

                            if data.services
                                for s in data.services
                                    if services.every((old)-> old.service isnt s.service and old.peer.id isnt s.peer.id)
                                        services.push s

                            if data.subscribers
                                _old = subscribers.map((p)->p.id)
                                for p in data.subscribers
                                    if p.id not in _old
                                        if @addPeer(p, false)
                                            defer.progress {subscriber: p}
                                            subscribers.push p
                                        else
                                            failedPeers.push p

                        .error (e) =>
                            if @opts.verbose
                                console.log "error! #{@me.id} add #{e.peer.id} to failed peers! (#{URI})"
                            failedPeers.push e.peer.id

                        .catch (e) =>
                            if @opts.verbose
                                console.log "catch! #{@me.id} add #{e.peer.id} to failed peers! (#{URI})"

                            failedPeers.push e.peer.id

                        .finally ->
                            if ++cnt is filtered.length
                                queriesSent += cnt
                                query()

            if filtered.length is 0
                if not @opts.nodelete and not @closed
                    for _id in failedPeers when fromWho[_id]
                        for from in fromWho[_id]
                            @msg(from, "/peers/#{_id}", {method: 'DELETE'})
                                .error ->
                                    console.log "error when trying to DELETE"

                defer.resolve {peers: closest, queriesSent, services, subscribers, hops}

        query()
        return defer.promise

     #
    # Public APIs that sends requests to other peers
    #

    getService: (id) =>
        if not isHash id
            id = hash id

        @_get(id, "/services/#{id}")
            .bind(@)

    getPeer: (id, concurrency) =>
        @_get(id, "/peers?sort=#{id}", concurrency)
            .bind(@)
            .error (e) =>
                if @opts.verbose
                    console.log "got error while doing getPeer:", e

    getSubscribers: (id) =>
        if not isHash id
            id = hash id

        @_get(id, "/services/#{id}/subscribers")
            .bind(@)
            .error (e) =>
                if true or @opts.verbose
                    console.log "got error while doing getSubscribers:", e

    publish: (data) ->
        #todo: optimize, add previously known subscription saver to first of list
        msgsSent = 0
        queries = 0
        fails = 0
        promises = []
        sTime = Date.now()
        queryTime = 0
        hopCount = 0
        sendTo = (peer) =>
            data.sent = Date.now()
            promises.push @msg(peer, "/data", {method:'POST', msg: JSON.stringify(data)})
                .catch ->
                    fails++
                .finally ->
                    msgsSent++

        @getSubscribers(hash data.service)
            .progressed ({subscriber}) =>
                sendTo subscriber
            .then ({subscribers, queriesSent, hops}) =>
                if @opts.verbose
                    console.log sty.blue "subscribers were"
                    console.log arguments

                hopCount += hops
                queries += queriesSent
                queryTime = Date.now() - sTime
                Promise.settle promises
            .then =>
                Promise.resolve {
                    peerQueries: queries,
                    msgs: msgsSent,
                    queryTime: queryTime,
                    postTime: Date.now() - sTime - queryTime,
                    hopCount: hopCount
                }

    subscribe: (URI) ->
#        console.log @me.id, "subscribe to", URI
        @subscribeHash hash(URI)

    _subscribe: (peer, _hash, who) =>
        who ?= @me

        if who is @me
            @mySubscriptions[_hash] ?= []
            @mySubscriptions[_hash].push peer

        #        console.log "send /subscribe/#{_hash} to #{peer.id} (#{peer.addr})"
        @msg(peer, "/services/#{_hash}/subscribers", {method: 'POST', msg: JSON.stringify who})
            .catch =>
                if @opts.verbose
                    console.log "failed to subscribe to peer #{peer.id}"

    subscribeHash: (_hash) ->
        @getPeer(_hash)
            .then ({peers}) =>
                _old = (old.id for old in @mySubscriptions[_hash] ?= [])

                promises = []
                for peer in peers[0..SUBSCRIBE_N] when peer.id not in _old
                    promises.push @_subscribe(peer, _hash)

                Promise.settle(promises)
                    .finally =>
                        @mySubscriptions[_hash] = sortByDistance(_hash, @mySubscriptions[_hash])[0..SUBSCRIBE_N]


    searchItem: (string, bucket=0, ID) =>
        defer = Promise.defer()

        ID ?= hash @me.id + Date.now()

        if ID in @_searchItemIDs
            defer.resolve {}
            return defer.promise

        @_searchItemIDs.push ID
        setTimeout =>
            @_searchItemIDs.shift()
        , 1000*60

        servicesMerged = @_searchLocal string
        mergeServices = (arr) ->
            keypeers = servicesMerged.map((s)-> s.service + s.peer.id)
            for service in arr when service.service + service.peer.id not in keypeers
                servicesMerged.push service

        itemsMerged = []
        mergeItems = (arr) ->
            signs = itemsMerged.map((i) -> i.sign)
            for item in arr when item.sign not in signs
                itemsMerged.push item



        if not @opts.test
            console.log "searching for #{string}"

        # use constrained broadcasting for searching items, only forward
        # search to peers which are at bucket whichs N is larger than the
        # one where this node was on searchers k-bucket
        peers = [].concat (list for n,list of @bucket when n > bucket)...

        cnt = -1
        resolve = ->
            if ++cnt is peers.length
                defer.resolve {items: itemsMerged, services: servicesMerged}

        @cache.get(string)
            .then(mergeItems)
            .finally resolve

        for peer in peers
            @msg(peer, '/search/'+string, {headers:{'p2p-bcast-id': ID, 'p2p-bcast-bucket': peer.bucket}})
                .then ({items, services, peers}) =>
                    #TODO: return peers instead of returning nothing
                    if not items
                        return

                    if bucket is 0
                        @cache.add items, false

                    defer.progress {items, services, peers}
                    mergeItems items
                    mergeServices services
                .finally resolve

        return defer.promise

    register: (services) ->
        if not Array.isArray services then services = [services]

        grouped = services.reduce((result, service) ->
            key = hash service.service
            service.id = key
            result[key] ?= []
            result[key].push service
            return result
        , {})

        keys = Object.keys(grouped)
        if keys.length > 1
            return Promise.map keys, (group) =>
                @register grouped[group]

        key = keys[0]
        services = grouped[key]
        @_addServices services

        @getPeer(key) #, 'register')
            .then ({peers}) =>
                row = @ownServices[key] ?= []
                peerIds = (peer.id for peer in row)

                _promises = []

                for peer in peers[0..SUBSCRIBE_N]
                    #prevent registering multiple times
                    if peer.id not in peerIds
                        for item in services
                            row.push {key: item.service, peer: peer, time: Date.now()}

                    _promises.push(@_register peer, services)

                return Promise.any _promises

    _register : (peer, services) ->
        if peer.id isnt @me.id
            _hash = hash services[0].service
#            console.log "store services", services ,"to peer #{peer.id}"
            @msg peer, "/services/#{_hash}", {method: 'POST', msg: JSON.stringify services}

    ping: (peer, onFail) ->
        onFail ?= -> return false
        @msg(peer, '/', {method: 'HEAD'})
            .then =>
                peer.lastSeen = Date.now()
            .error(onFail)
            .catch(onFail)


module.exports = p2p

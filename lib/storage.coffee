{pat2regx} = require './utils'
EventEmitter = require('events').EventEmitter
Promise = require 'bluebird'
nedb = require('nedb')

class storage extends EventEmitter
    DB: null

    EXPIRE: 1000*60*60*24*8

    loading: 0 # 0: not started, 1: loading, 2: done

    # opts:
    #   persistent (bool) save & read file to & from disk | default false
    #   filename (string) file to where save data
    constructor: (opts={}) ->
        {persistent, filename} = opts
        if persistent
            filename ?=  __dirname+'/../logs/storage.db'

        @DB = new nedb {filename, isMemoryOnly: not persistent, autoload: true}
        @DB = Promise.promisifyAll @DB

        @DB.ensureIndex {fieldName: 'data.service'}
        @DB.ensureIndex {fieldName: 'hash'}

    get: (URI="*") ->
        defer = Promise.defer()
        @DB.find({service: pat2regx(URI)}).sort({time:-1}).limit(1000).exec (err, docs)->
            if err
                return defer.reject()

            defer.resolve docs
        return defer.promise

    add: (data, local) =>
        if Array.isArray data
            promises = (@add d, local for d in data)
            return Promise.settle promises

        @emit 'data', data, local

        @DB.findAsync({sign: data.sign})
            .then (res) =>
                if res.length is 0
                    return @DB.insertAsync data



module.exports = storage


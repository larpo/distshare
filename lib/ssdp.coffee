HEADER = "HTTP/1.1 200 OK\r\n"
SEARCH_HEADER = "M-SEARCH * HTTP/1.1\r\n"

stringify = (msg) ->
    DEFAULTS =
        host: SSDP.ADDR + ":" + SSDP.PORT
        header: HEADER

    merge msg, DEFAULTS

    s = msg.header
    delete msg.header

    s += "HOST:#{msg.host}\r\n"
    delete msg.host

    for key, val of msg
        s += key.toUpperCase() + ":#{val}\r\n"

    return s + "\r\n"

merge = (obj, def = {}) ->
    for key, value of def
        obj[key] = obj[key] || value

    return obj

SSDP = {}

SSDP.search = (s) ->
    msg =
        header: SEARCH_HEADER
        st: s
        mx: 1
        man: "\"ssdp:discover\""

    return stringify msg

SSDP.advertise = (uuid, service, opts) ->
    msg =
        nts: "ssdp:alive"
        usn: "uuid:#{uuid}::#{service}"
        "cache-control": "max-age=3600"
        ext: ""



    msg = stringify merge(msg, opts)
    return msg

SSDP.respond = (uuid, service, opts) ->
    opts = ST
    return SSDP.advertise(uuid, service, opts)

SSDP.parse = (msg) ->
    results = {}
    rows = msg.split("\r\n").reverse()
    if rows.pop().match(/M-SEARCH/i)
        results.type = "search"

    while rows.length
        row = rows.pop().split(":")
        results[row.splice(0,1)] = row.join(":")

    return results

SSDP.PORT = 1900
SSDP.ADDR = "239.255.255.250"

module.exports = SSDP
"use strict"
io = require 'socket.io-client'
{discover} = require __dirname + '/../lib/discover'
{EventEmitter} = require 'events'
{fetchUrl,hash, dateTime, sign} = require './utils'

"""
    Events are put into objects that are formatted as follows
    {
        service: 'servicename'
        hash: 'sha1 signature'
        sign: 'sha1 signature'
        time: 'time emitted, unixtime'
        data: { event data }
    }
"""
class Bot extends EventEmitter
    SERVICE: "BOT"
    KEYWORDS: []
    DESCRIPTION: ""

    debug: false

    socket: null
    online: false
    connecting: false

    constructor: ->
        @msgBuffer = []
        @servers = []

        @on "connect", @onConnect
        @on "msg", @onMsg
        @on "data", @onData
        @on "error", console.error

        @discover()

    discover: =>
        @servers = []
        discover()
            .progressed (server) =>
                if server.local and not @connecting and not @online
                    @connect server

                @servers.push server
            .catch (msg) =>
                @emit "error", msg
            .then =>
                if @servers.length > 0 and not @connecting and not @online
                    @connect @servers[0]
                else if @servers.length is 0
                    setTimeout @discover, 30 * 1000



    connect: ({addr,port}) =>
        @connecting = true
        reconnect = =>
            @online = false
            @discover()

        @socket = io.connect(addr, {port, reconnect: false, 'force new connection':true})
            .on "connect", (data) =>
                console.log "connected to #{addr}:#{port}"
                @online = true
                @connecting = false
                while @msgBuffer.length > 0
                    msg =  @msgBuffer.shift()
                    @_emit msg...

                @emit "connect", data

            .on "data",  (data) =>
                @emit "msg", data

            .on "connect_failed", () =>
                @emit "error", "could not connect to server"
                reconnect()

            .on "error", (err) =>
                @emit "error", "socket failed"
                reconnect()

            .on "disconnect", () =>
                console.log "lost connection to server"
                reconnect()

    sign: (msg) =>
        return sign msg

    hash: (msg) =>
        return hash msg

    _emit: (msg...) =>
        if @online
            @socket.emit msg...
            console.log "socket emits", msg...
        else
            @msgBuffer.push msg

    register: (service) =>
        if not service
            service =
                service: @SERVICE
                description: @DESCRIPTION
                keywords: @KEYWORDS

        @_emit "register", service

    broadcast: (data, service=@SERVICE) =>
        _sign = @sign data
        _hash = @hash data
        @_emit "data", {service, data, sign: _sign, hash: _hash, time: Date.now()}

    msg: (data, to) =>
        _sign = @sign data
        _hash = @hash data
        @_emit "data", {to, data, sign: _sign, hash: _hash, time: Date.now()}

    onConnect: (data) =>
        @register()

    onMsg: ({data, room}) ->
        console.log data, room

    onData: (data) ->

    formatTime: dateTime


class PollBot extends Bot
    INTERVAL: 1000*60*20 #20mins

    _lastItemHash: ""

    constructor: (args...) ->
        super args...
        @_poll()

    _pollTimeout: null

    _poll: =>
        @_pollTimeout = setTimeout @_poll, @INTERVAL
        @poll()

    poll: =>

    onData: (data) ->
        if @debug then console.log "new data", data

        data = @format data

        if not data or data.length is 0
            return

        if not Array.isArray data
            data = [data]

        newItems = []
        for item in data
            h = hash JSON.stringify(item)
            if @_lastItemHash is h
                break

            newItems.unshift item

        for item in newItems
            @broadcast item

        @_lastItemHash = hash JSON.stringify(data[0])


    format: (data) -> data

class UrlBot extends PollBot
    URL: ""
    _lastFetch: null
    _hash : ""

    parse: (data) -> data

    poll: =>
        handle = ({data, time}) =>
            @_lastFetch = time
            _hash = hash data
            if _hash isnt @_hash
                @_hash = _hash
                @emit 'data', @parse data

            return

        _error = (msg) =>
            @emit "error", msg

        console.log new Date(), "fetching url", @URL
        fetchUrl({url:@URL, lastTime:@_lastFetch})
            .catch(_error)
            .then(handle)
            .done()

module.exports = {Bot, PollBot, UrlBot}
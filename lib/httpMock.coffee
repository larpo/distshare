{EventEmitter} = require 'events'
stream = require 'stream'

servers = {}
sockets = {}

class mockReq extends EventEmitter
    opts: null
    headers: null
    url: ""
    ended: false
    buffer: null
    constructor: (opts={}, @delay) ->
        @buffer = []
        @headers = {}
        if opts?.path
            @url = opts.path

        for key, val of opts
            @[key] = val


    setHeader: (key, val) ->
        @headers[key] = val

    write: (chunk) =>
        if @listeners('data').length
            @emit 'data', chunk
        else
            @buffer.push chunk
            @once 'newListener', (name, fn) =>
                if name is 'data' and @buffer.length
                    while @buffer.length
                        fn @buffer.shift()

                    process.nextTick @end

    error: (err) ->
        setTimeout =>
            @emit 'error', err
        , @delay
#        process.nextTick =>
#            @emit 'error', err

    end: (chunk) =>
        if chunk
            @write chunk

        if not @buffer.length
#            @emit 'end'
            setTimeout =>
                @emit 'end'
            , @delay

        return @


class Server extends EventEmitter
    constructor: (@func) ->

    listen: (@port, addr) =>
        if not servers[@port]
            servers[@port] = @func
            @emit 'listening'
        else
            @emit 'error', {code: 'EADDRINUSE'}

        return @

    close: (fn) ->
        delete servers[@port]
        @removeAllListeners 'error'
        @removeAllListeners 'listening'
        if fn
            fn()

class HTTP
    opts: null
    delay: 0
    msgs: 0
    deletes: 0
    posts: 0
    heads: 0

    constructor: (@opts={})->
        @delay = @opts?.delay || 0

    createServer: (func) ->
         return new Server(func)

    request: (opts, func) ->
        @msgs++
        method = opts.method?.toLowerCase()
        if method is "delete"
            @deletes++
        else if method is "post"
            @posts++
        else if method is "head"
            @heads++


        req = new mockReq(opts, @delay)

        if servers[opts.port]
            res = new mockReq({statusCode:200}, @delay)

            servers[opts.port](req, res)
            func(res)
            return req
        else
            req.error {code: 'EACCESS'}
            return req


module.exports = HTTP

if require.main is module
    console.log "Testing mock http"

    http = new HTTP()
    srv = http.createServer (req, res) ->
        _d = ""
        req.on 'data', (__d) ->
            _d += __d

        req.on 'end', ->
            console.log "data from req", _d

        console.log "received request!", res
        res.end "ping!"

    srv.on 'listening', ->
        console.log "listening"

    srv.listen 6666

    req = http.request {port: 6666}, (res) ->
        console.log "making request! response: ", res
        d = ""

        res.on "data", (data) ->
            d += data
        res.on 'end', ->
            console.log "response ended", d


    req.end 'foo'
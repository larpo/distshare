#ADHOC Botnet

## How to Install

###Automagically in raspberry pi
```
curl https://larpo.kapsi.fi/adhoc.sh | sh
```

###Locally
```
git clone git@bitbucket.org:larpo/botnet.git
cd botnet
npm install
```


## Running the whole system
```
npm start
```

### without source bots
```
coffee server.coffee
```

note: if the device has multiple interfaces, udp multicast might need to be configured to route
multicasts to the interface which is connected to the network with devices. for example

```
sudo route add -net 224.0.0.0 netmask 240.0.0.0 dev eth1
```

##about the system
The backend of the system uses a simple service registeration and subscription API.

Service providers (bots) are expected to announce services that they are providing.
The most important important piece of information in the announce message is name
of the service. The name consists of a well formed case insensitive URI
which will be used for accessing the service.

For example weather bot providing information about weather in Sendai should register
its service by sendin server a 'register' message that could have following content:

    {
        service: "/weather/sendai",
        source: "http://forecast.io",
        description: "Weather in Sendai.",
        keywords: ["weather", "sendai"]
    }

Clients can subscribe to these services by sending a the URI of the service in a
'subscribe' message to the server. Subscription supports wild cards for subscribing to
multiple services at once. For example client interested in all weather information could
subscribe to URI '/weather/ *'. Similarily URI '/ * /sendai' would subscribe to all information
conserning sendai.

Searching services can be done using both keywords and service URI.


## P2P implementation
Kademlia based DHT implementation that uses HTTP requests as transport protocol

####Bootstrap:
* initiate peer search on OWN id in order to find closest peers and fill routing table

####Peer search:
1. send query to k peers closest to the id asking them for k peers closest to id
2. take k closest peers from the results and repeat 1 until no better peers are returned

####Service search by ID (hashed URI):
* like peer search, but return also services that match the ID

####Service/item search by plain text/regex (not part of kademlia):
* choose a peer randomly from each bucket and send a query
* put bucket number where the peer was to the message
* forward query only to peers with greater bucket number

####Store key, value:
1. find peer closest to the key id using peer search
2. send store message the peer

####Maintenance:
* ping peers in k-bucket every T seconds
* push local and directly assigned data to N closed peers every T
* republish data every T
* purge old data from key-value list every 2T


####T & k:
* could be defined adaptively based on churn and peer number
* for now, let T be 30s and k = 5


SERVICES: the distributed hash table of services

    sha1id... : [
        {
            service: "key in plain text"
            description: "optional description text"
            peer: {...peer description...}
            expires: exirary time
        }
    ]

OWN_SERVICES: hash table for own services

 * like SERVICES, but stores ids of the nodes where the data has been pushed
 * helps refreshing the entries


BUCKET: k-bucket of peers

    1: [
            //least recently seen peer is kept at first position
        {   //peer description
            port: 80
            ip: 192.168.1.50
            id: sha1hash
            lastSeen: timestamp
        }
    ]


HTTP REQUEST URLS for the p2p system

 * all URLs are prefixed with /p2p (eg. /p2p/ping )
 * Each message sent to other peer has peer ID and listenning port number on the HEADER
  - p2p-peer-id
  - p2p-peer-port

PING:

    GET /ping
    * response headers must have status code of 200, body is optional

SEARCH PEER

    GET /peer/:peerId
    * response body contains JSON encoded array of k peers closer to the asked peer ID
    * return all known peer is peerId is not provided

SEARCH SERVICE

    GET /service/:serviceId
    * response body contains JSON encoded object with two arrays
     - 'peers' array that contains k peers closer to the id
     - 'services' array that contains values(location) of stored services that match the ID

SET KEY,VALUE

    POST /service/:serviceId
     * request body contains JSON list of services of the data to be stored
       {
            service: "plaintext key/URI to be stored as hash(key)"
            peer: {...}
       }

SEARCH STRING (recursive)

    GET /search/:keywords with possible wildcards
    * response header contains which bucket the node was, and a query specific ID
    * the query is then forwarded to nodes with greater bucket number only if the ID is new

    * response body contains list of matching local items and services as well as a tree of visited peers


SUBSCRIBE TO KEY

    POST /subscribe/:serviceId (and DELETE /subscribe/:serviceId/:nodeId ?)
    * request body contains node information
    * nodes publishing new data should GET /subscribe/:serviceId to get list of nodes interested in data

    GET  /subscribe/:serviceId
    * all peers subscribed to id


##kad optimizations
* Prefer nodes with lowest RTT in buckets
* Cache nodes where subscriptions are registered
* Inactive spare peers to fill out gaps when peers fail (not active before that)
* Piggyback recent churn info (peer estimate, churn rate, failed peers) into normal messages
* self-tuning based on peer estimate and churn rate (maintanance interval, subscription distribution factor)

* Subscribe / publish model:
* "power of two choises" paradigm
- three possible modes: fast publishing, fast subscription and added reliability
- fast publish:
  - subscriptions saved to N different nodes, N depends on network size (log N?)
    - alternatively peers replicate their subscription lists to others responsible?
  - subsribers registers to all peers corresponding to hash values of the service
  - publishers pick randomly one of the nodes maintaining list of subscribers

* when STORE/GET, the node responsible returns reference for closer nodes if one exists

* adaptive registering mode:
  - the number of nodes responsible for a url depends on the generality/popularity of the url
    - based on length: for example 10 nodes for / , 4 for /weather and 2 for /weather/oulu
  - keep track of nodes where registeration was done, reregister if responsible node changes


* expected frequency of events on the system, bots >> clients:
  * PUBLISH, most frequent, M times per hour * N number of bot nodes
  * REGISTER, once for every N bot, refreshed every T hours
  * SUBSCRIBE, few times for every C client
  * SEARCH, few times for every C client



#REST APIs

##P2P

###Piggybacking peer management data in headers
Some data can be piggybacked to reguests

* peer information of the client node, "p2p-peer-id" and "p2p-peer-port"
* information about recent new or failed nodes
* information about nodes that are closer to requested service
* information about reqursive search path and nodes on the path

###/p2p/stats
    GET
       Returns estimated stats of the p2p network
         * nodes estimate of total peers (based on neighbor density)
         * nodes estimate of churn rate

###/p2p/peers{?limit=N&sort=ID}
    GET
       Returns all peers
       options:
        * limit (N) - limit to N peers
        * sort (ID) - sorts peers by ID

    POST
        Add peer to node
       403 Forbidden if bucket is full

###/p2p/peers/ID
    GET
        Returns information about peer (contact, resources)

    DELETE
        Delete peer from node

###/p2p/services (?search=fulltextSearchString)
    GET
        Returns list of known services
        options:
         * search (string) - fulltext search string
    POST (overlaps with POST /p2p/services/sID ?)
        Register new service

###/p2p/services/serviceId
    GET
        Returns list of nodes providing corresponding service.
        Expand also subscribers?

    POST
        Register new service

###/p2p/services/serviceId/subscribers
    GET
        Returns list of peers and  callback URLs for the service.
    POST
        Register new subscription callback URL

###/p2p/services/serviceId/subscribers/nodeId
    DELETE
        Unsubscribes the node for service



